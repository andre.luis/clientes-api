package com.andre.clientes.services;

import com.andre.clientes.entities.Client;
import com.andre.clientes.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class ClientService {

    @Autowired
    private ClientRepository repository;

    @Transactional(readOnly = true)
    public Page<Client> findAllPaged(PageRequest pageRequest) {
        Page<Client> list = repository.findAll(pageRequest);
        return list;
    }

    @Transactional(readOnly = true)
    public Client findById(Long id) {
        Optional<Client> obj = repository.findById(id);
        Client entity = obj.get();
        return entity;
    }

    @Transactional(readOnly = true)
    public Client insert(Client client) {
        client.setId(null);
        return repository.save(client);
    }

    @Transactional(readOnly = true)
    public Client update(Long id, Client client){
        Client entity = repository.getOne(id);
        entity.setName(client.getName());
        entity = repository.save(entity);
        return entity;
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }
}
